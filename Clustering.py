from pyclustering.cluster.kmedoids import kmedoids
from pyclustering.utils.metric import type_metric, distance_metric
from pyclustering.utils import read_sample;

from pyclustering.cluster import cluster_visualizer;
from ComputeSimilarity import main
from Multithreading import generate_distance_matrix
import json
import random
import pandas as pd

file="C:/Users/aprialdi.pratama/Downloads/sku_infoxx.json"
with open(file) as f:
     sampleall = json.load(f)

sample=sampleall[:2000]

def get_initial(n, max):
    result=[]
    for x in range(n):
        y=random.randint(1, max-1)
        while y in result:
            y = random.randint(1, max - 1)
        result.append(y)
    print("Initial Medoids: ")
    print(result)
    return result

import time

current_milli_time = lambda: int(round(time.time() * 1000))

number = input("Enter the desired number of clusters: ")
initial_medoids = get_initial(int(number), len(sample))
time_start=current_milli_time()


def clustering(metric, data, initial):
    kmedoids_instance = kmedoids(data, initial, metric=metric)
    kmedoids_instance.process();
    return kmedoids_instance

user_function = lambda point1, point2: main(point1,point2);
metric = distance_metric(type_metric.USER_DEFINED, func=user_function);
kmedoids_instance = kmedoids(sample, initial_medoids, metric=metric)
kmedoids_instance.process();
clusters = kmedoids_instance.get_clusters()
medoids = kmedoids_instance.get_medoids()

def get_medoids(medoids, dataset):
    results=[]
    for m in medoids:
        results.append(dataset[m].get("sku"))
    return results

med=get_medoids(medoids,sample)
#print("Medoids: ")
#print(med)

def get_top_n(n, clusters, medoids, dataset):
    results=[]
    for t in zip(clusters, medoids):
        members=t[0]
        medoid=t[1]
        dist=[]
        for m in members:
            distance=metric(dataset[m],dataset[medoid])
            dist.append((m, distance))
        dist.sort(key=lambda tup: tup[1])
        results.append(dist[:n])
    return results

#result=get_top_n(1,clusters, medoids, sample)
#print(result)

def clusteringMatrix(matrix, initial):
    kmedoids_instance = kmedoids(matrix, initial, data_type='distance_matrix')
    kmedoids_instance.process()

    return kmedoids_instance

def membership(clusters, medoids, dataset):
    result=[]
    for t in zip(clusters, medoids):
        members=t[0]
        medoid=t[1]
        length=len(t[0])
        sum_distance=0
        max_distance=0
        min_distance=999999
        sku_members=[]
        for m in members:
            distance=metric(dataset[m],dataset[medoid])
            sum_distance+=distance
            if distance<min_distance:
                min_distance=distance
            if distance>max_distance:
                max_distance=distance
            sku_members.append(dataset[m].get("sku"))
        average=sum_distance/float(length)
        result.append((sku_members,average,max_distance,min_distance))
    return result

member=membership(clusters, medoids, sample)
#print("Detail info for each clusters: ")
#print(member)

def visualize(clusters, initial, medoids):
    visualizer = cluster_visualizer(1);
    visualizer.append_clusters(clusters, sample, 0);
    visualizer.append_cluster([sample[index] for index in initial], marker='.', markersize=15);
    visualizer.append_cluster(medoids, data=sample, marker='*', markersize=15);
    visualizer.show()

#visualize(clusters,initial_medoids,medoids)

def write_output(clusters, medoids, sample):
    elements=[]
    cols=['sku_cluster','sku_member']
    med = get_medoids(medoids, sample)
    members = membership(clusters, medoids, sample)
    for m in zip(med,members):
        medoid=m[0]
        clusters=m[1]
        members=clusters[0]
        for mb in members:
            elements.append((medoid,mb))
    df=pd.DataFrame(elements,columns=cols)
    df.to_csv("C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/membership.csv")

def run_clusteringMatrix(matrix, n, data):
    initial=get_initial(n, len(data))
    clustering_instance=clusteringMatrix(matrix, initial)
    clusters = clustering_instance.get_clusters()
    medoids = clustering_instance.get_medoids()
    write_output(clusters,medoids,data)

matrix=generate_distance_matrix(file)
run_clusteringMatrix()
time_finish=current_milli_time()
print("Number of Data: "+str(len(sample)))
print("Time: ",time_finish-time_start)

