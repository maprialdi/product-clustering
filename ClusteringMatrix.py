from pyclustering.cluster.kmedoids import kmedoids
from pyclustering.utils.metric import type_metric, distance_metric
from pyclustering.samples.definitions import FCPS_SAMPLES
from pyclustering.utils import calculate_distance_matrix, read_sample
import csv
import numpy as np
import random
import json
import pandas as pd

def read_data(file):
    with open(file) as f:
        sampleall = json.load(f)
    return sampleall

def get_initial(n, max):
    print("Getting initial medoids...")
    result=[]
    for x in range(n):
        y=random.randint(1, max-1)
        while y in result:
            y = random.randint(1, max - 1)
        result.append(y)
    #print("Initial Medoids: ")
    #print(result)
    return result

def readcsv(file):
    matrix=np.genfromtxt(file, delimiter=',', dtype=None)
    np.set_printoptions(formatter={'float_kind': '{:f}'.format})
    return matrix

def clusteringMatrix(matrix, initial):
    print("Building clusters...")
    kmedoids_instance = kmedoids(matrix, initial, data_type='distance_matrix')
    kmedoids_instance.process()

    return kmedoids_instance

def get_medoids(medoids, dataset):
    results=[]
    for m in medoids:
        results.append(dataset[m].get("sku"))
    return results

def membership(clusters, medoids, dataset):
    result=[]
    for t in zip(clusters, medoids):
        members=t[0]
        medoid=t[1]
        length=len(t[0])
        sum_distance=0
        max_distance=0
        min_distance=999999
        sku_members=[]
        for m in members:
            sku_members.append(dataset[m].get("sku"))
        average=sum_distance/float(length)
        result.append((sku_members,average,max_distance,min_distance))
    return result

def write_output(clusters, medoids, sample):
    elements=[]
    cols=['sku_cluster','sku_member']
    med = get_medoids(medoids, sample)
    members = membership(clusters, medoids, sample)
    for m in zip(med,members):
        medoid=m[0]
        clusters=m[1]
        members=clusters[0]
        elements.append((medoid,members))
    df=pd.DataFrame(elements,columns=cols)
    df.to_csv("C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/membership-raw.csv")

if __name__ == "__main__":
    inputFile="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/raw.json"
    inputCsv="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/distance_matrix_raw.csv"
    matrix=readcsv(inputCsv)
    #matrix=generate_distance_matrix(inputFile)

    data=read_data(inputFile)
    #number = input("Enter the desired number of clusters: ")
    number=25
    initial = get_initial(int(number), len(data))
    clustering_instance = clusteringMatrix(matrix, initial)
    clusters = clustering_instance.get_clusters()
    medoids = clustering_instance.get_medoids()
    write_output(clusters, medoids, data)