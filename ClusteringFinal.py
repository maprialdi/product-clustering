import pandas as pd
from google.cloud.bigquery import SchemaField
from Sampling import run_clustering, get_membership
from data import data_bmd
import json

def readcsv(input):
    df=pd.read_csv(input)
    return df['medoid'].tolist()

def getskuinfo(medoids):
    df=pd.DataFrame()
    for m in medoids:
        query = """
              select
                sku, case when UnitPrice <= 0 then 1 else UnitPrice end as UnitPrice, brand, name, description
                , trim(concat(Category_Name_Level_1, ' ', ifnull(Category_Name_Level_2, ''), ' ' , ifnull(Category_Name_Level_3, '') , ' ' , ifnull(Category_Name_Level_4, ''))) as category
                , qty
              from
                `stg.temp_cluster`
              where
                sku=@SKU
        """

        param = [{
            'field': 'SKU'
            , 'type': 'STRING'
            , 'value': m
        }]

        ds = data_bmd(10000)
        ls = ds.bq_query(query=query, param=param)
        df=df.append(ls)
    return df


input="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/membership-weekly-all.csv"
output="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/membership-clustering-final.csv"
df=readcsv(input)
skus=getskuinfo(df)
sampling_json = skus.to_json(orient='records')
data = json.loads(sampling_json)
num_clusters=2
medoids=run_clustering(data, num_clusters)
get_membership(data, medoids, output)
print(medoids)
