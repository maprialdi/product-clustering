from pyclustering.utils.metric import type_metric, distance_metric;
from fuzzywuzzy import fuzz
import math
import numpy as np
import json
import time
import pandas as pd
from sdtw import SoftDTW
from sdtw.distance import SquaredEuclidean

current_milli_time = lambda: int(round(time.time() * 1000))
#nlp=spacy.load('xx')
def read_file(input):
    df=pd.read_json(input, lines=True)
    sampling = df.sample(n=1000)
    sampling_json = sampling.to_json(orient='records')
    data = json.loads(sampling_json)
    #print(len(df))
    return data

def normalized_timeseries(t):
    time=list(map(int, t))
    result=[]
    low=min(time)
    high=max(time)
    diff=float(high-low)
    if diff==0:
        sum=np.sum(time)
        for t in time:
            y=float(t)/float(sum)
            result.append(y)
    else:
        for t in time:
            y=float(t-low)/diff
            result.append(y)
    return result

def compute_dtw_distance(timeseries1, timeseries2):
    gamma=0.01
    ts1_split=timeseries1.split(',')
    ts2_split = timeseries2.split(',')
    ts1=np.array(ts1_split).astype(np.float)
    ts2=np.array(ts2_split).astype(np.float)
    mean_data=(np.sum(ts1)+np.sum(ts2))/(len(ts1)+len(ts2))
    #normalized1=normalized_timeseries(timeseries1)
    #normalized2=normalized_timeseries(timeseries2)
    if mean_data==0:
        normalized1=ts1
        normalized2=ts2
    else:
        normalized1=ts1/mean_data
        normalized2=ts2/mean_data
    time_start = current_milli_time()
    D = SquaredEuclidean(normalized1.reshape(-1,1), normalized2.reshape(-1,1))
    sdtw = SoftDTW(D, gamma=0.01)
    # soft-DTW discrepancy, approaches DTW as gamma -> 0
    d = sdtw.compute()
    #d=metrics.soft_dtw(normalized1,normalized2, gamma)
    #dist = abs(distance)
    distance=abs(d)
    dist=0
    if distance!=0:
        dist=math.log10(distance)
    else:
        dist=distance
    #time_finish = current_milli_time()
    #print("Computation DTW time: ", time_finish - time_start)
    #print('qty distance', distance)
    #print(dist, normalized1.reshape(-1,1), normalized2.reshape(-1,1), d)
    return abs(dist)

def log_function(price):
    if price<1:
        result=0
    else:
        result= math.log10(abs(float(price)))
    return result

def compute_price_distance(price1, price2):
    log1=log_function(price1)
    log2=log_function(price2)
    #print('price distance', math.fabs(log1-log2))
    return math.fabs(log1-log2)

def compute_features_distances(features1, features2, distance_operators, weight):
    result={}
    for key, value1 in features1.items():
        value2=features2[key]
        op=distance_operators.get(key,None)
        if op:
            d=op(value1,value2)
            w=weight[key]
            #result[key]=d*w
            result[key]=d
    return result

def compute_similarity(features_distances):
    result=0.0
    for key, value in features_distances.items():
        result=result+value
    return result

def compute_similarity_token_sort_ratio(txt1,txt2):
    distance=float(100-fuzz.token_sort_ratio(txt1, txt2))/100.0
    #print('token distance', distance)
    return distance

def main(features1, features2):
    #distance_operators = {'UnitPrice': compute_price_distance,
                          #'description':compute_similarity_token_sort_ratio, 'category': compute_similarity_token_sort_ratio}

    distance_operators = {'UnitPrice': compute_price_distance, 'qty': compute_dtw_distance,
                          'description':compute_similarity_token_sort_ratio, 'category': compute_similarity_token_sort_ratio}
    weights = {'UnitPrice': 0.2, 'category': 0.3, 'qty': 0.2, 'description':0.3}
    features_distance=compute_features_distances(features1, features2, distance_operators, weights)
    #print('features distance', features_distance)
    similarity = compute_similarity(features_distance)
    return similarity, features_distance

if __name__ == "__main__":
    user_function = lambda point1, point2: main(point1,point2);
    metric = distance_metric(type_metric.USER_DEFINED, func=user_function);
    features1={"qty": [4, 3], "UnitPrice": 2583636.0, "category": "Aksesoris Gadget & Komputer","description": "Deskstar NAS 6TB"}
    features2={"qty": [2, 2, 1, 1], "UnitPrice": 4573182.0,"category": "Aksesoris Gadget & Komputer","description": "Deskstar NAS 8TB"}
    file="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/sampling_5pct.json"
    time_start = current_milli_time()
    sampleall=read_file(file)
    data_i=sampleall[0]
    processing_fastdtw=0
    for s in sampleall:
        distance, proc = metric(s, data_i)
        print(proc)
        #processing_fastdtw+=proc
    time_finish = current_milli_time()
    print("Processing Time: ", time_finish - time_start)
    print("Processing FastDTW: ", processing_fastdtw)
# print("Distance: ", distance)
# print("Feature Distances: ", features)