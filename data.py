import pandas as pd
import numpy as np
from google.cloud import bigquery
from google.cloud import storage
from sklearn.preprocessing import MinMaxScaler

class data_bmd:
    KEY_PATH = 'C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/bmdbi-2257afae6fce.json'
    DATASET = 'stg'

    def __init__(self, sample=10000):
        self.sample = sample
        self.client = bigquery.Client.from_service_account_json(self.KEY_PATH)
        self.storage_client = storage.Client.from_service_account_json(self.KEY_PATH)

    def get_category_balance(self):
        query = """
            select
                category_name_level_1
                , category_id_level_1
                , count('') as val
              from
                `stg.trx_invecomputer` item
                inner join (
                  select
                    categ.*
                  from (
                    select
                      item_no_
                      , max(entry_no) as id
                    from
                      `stg.bi_category_history` cat
                    group by
                      item_no_
                  ) as latest 
                  inner join `stg.bi_category_history` categ on categ.item_no_ = latest.item_no_
                    and categ.entry_no = latest.id
                ) as coll on coll.item_no_ = item.vpartid
              where 
                vactivation = 2
                and category_name_level_1 not in (
                  'Internal'
                )
              group by
                category_name_level_1
                , category_id_level_1;
        """
        data = self.bq_query(query=query)
        total = data['val'].sum()
        data['portion'] = round(data['val'] / total * self.sample).astype(int)
        return data

    def bq_query(self, query, param=[]):
        query_params = []
        for x in range(len(param)):
            query_params.append(bigquery.ScalarQueryParameter(param[x]['field'], param[x]['type'], param[x]['value']))

        job_config = bigquery.QueryJobConfig()
        job_config.query_parameters = query_params
        data = self.client.query(
            query,
            job_config=job_config
        ).to_dataframe()
        return data

    def bq_insert(self, data, table_name, index):
        data.set_index(index, inplace=True)
        dataset = self.client.dataset(self.DATASET)
        table_ref = dataset.table(table_name)
        job = self.client.load_table_from_dataframe(data, table_ref)
        return job.result()

    def bq_stream(self, data, table_name, schema):
        dataset = self.client.dataset(self.DATASET)
        table_ref = dataset.table(table_name)
        table = self.client.get_table(table_ref)
        errors = self.client.insert_rows(table, data, schema)
        return errors

    def get_data_sku(self, sku, type):
        query = """
            select
              date(min(date)) as min_date
              , date(max(date)) as max_date
            from
              `stg.bi_daily_summary`
            where
                sku = @SKU
        """
        param = [{
            'field': 'SKU'
            , 'type': 'STRING'
            , 'value': sku
        }]
        min_max = self.bq_query(query=query, param=param)
        q_data = """
            with max as (
              select 
                max(date)
              from
                `stg.bi_daily_summary`  
              where
                channel = 'ShoppingCart'
            )
            select 
              stream.bom
              , ifnull(data.qty, 0) as qty
              , ifnull(data.perc_discount, 0) as perc_discount
            from (
                select 
                  date(TIMESTAMP_TRUNC(stream.date, @DWK)) as bom
                from
                  `stg.bi_datestream` stream
                where
                  date between '""" + min_max.min_date[0].strftime('%Y-%m-%d') + """' and (select * from max)
                group by
                  bom
              ) as stream
              left join (
                select
                  date(TIMESTAMP_TRUNC(summ.date, @DWK)) as bom
                  , sum(summ.qty) as qty
                  , case
                      when sum(Amount) <> 0 then sum(itm.Discount) / sum(Amount)
                      else 0
                    end as perc_discount
                from
                    `stg.bi_daily_summary` summ
                    inner join `stg.bmd_sales_invoice_header` inv on inv.No_ = summ.Document_No
                    inner join `stg.bhx_order` ord on ord.kodetrx = inv.External_Document_No_
                    inner join `stg.bhx_orderitem` itm on itm.kodetrx = ord.kodetrx
                where
                  channel = 'ShoppingCart'
                  and SKU = @SKU
                group by
                  bom
              ) as data on data.bom = stream.bom
            order by
              bom asc
        """
        q_week = q_data.replace('@DWK', 'WEEK')
        q_day = q_data.replace('@DWK', 'DAY')
        data_week = self.bq_query(query=q_week, param=param)
        data_day = self.bq_query(query=q_day, param=param)

        data_week['qty'] = round(data_week['qty'] / 100, 2)
        data_day['qty'] = round(data_day['qty'] / 100, 2)
        # data_week['qty'] = sc.fit_transform(data_week['qty'])
        data_week['qty_ma'] = round(data_week['qty'].rolling(window=5).mean(), 2)
        data_week['bom'] = pd.to_datetime(data_week['bom'], format='%Y-%m-%d')
        data_week = data_week.set_index('bom')

        data_day['bom'] = pd.to_datetime(data_day['bom'], format='%Y-%m-%d')
        data_day = data_day.set_index('bom')

        for s in range(1, 5):
            data_week['qty_week_{}'.format(s)] = round(data_week['qty_ma'].shift(s), 2)
            data_week['qty_ori_week_{}'.format(s)] = round(data_week['qty'].shift(s), 2)
            data_week['perc_discount_week_{}'.format(s)] = round(data_week['perc_discount'].shift(s), 2)

        data_day['qty_ma'] = round(data_day['qty'].rolling(window=5).mean(), 2)
        for s in range(1, 7):
            data_day['qty_day_{}'.format(s)] = round(data_day['qty_ma'].shift(s), 2)

        data_week['qty+1'] = data_week['qty'][6:]
        data_week['qty+1'] = data_week['qty+1'].shift(-1)
        data_week = data_week.iloc[5:]
        data_week['diff'] = data_week['qty+1'] - data_week['qty_ma']
        data_week['stat'] = np.where(data_week['diff'] > 0.1, 1, np.where(data_week['diff'] < -0.1, -1, 0))
        data_week = data_week[[
            'qty_ma', 'qty_week_4', 'qty_week_3', 'qty_week_2', 'qty_week_1'
            , 'perc_discount', 'perc_discount_week_4', 'perc_discount_week_3', 'perc_discount_week_2',
            'perc_discount_week_1'
            , 'diff']]
        data_week.fillna(0, inplace=True)
        data = pd.merge(data_week, data_day, on='bom', how='left')
        data = data.drop(['qty', 'perc_discount_y'], axis=1)
        data = data[[
            'qty_ma_x', 'qty_week_4', 'qty_week_3', 'qty_week_2', 'qty_week_1'
            , 'perc_discount_x', 'perc_discount_week_4', 'perc_discount_week_3', 'perc_discount_week_2',
            'perc_discount_week_1'
            , 'qty_ma_y', 'qty_day_1', 'qty_day_2', 'qty_day_3', 'qty_day_4', 'qty_day_5', 'qty_day_6', 'diff'
        ]]
        if type == 'production':
            data = data.tail(1)
            data = data.drop(['diff'], axis=1)
        return data

    def get_list_sku(self, start=0, limit=500):
        query = """
            select
              *
            from (
              select
                ROW_NUMBER() OVER(order by sort desc, sku asc) as no_
                , *
              from 
                `stg.stock_list_sku`
              order by 
                sort desc,
                sku asc
            ) as temp 
            where
              not exists (select '' from `stg.stock_training_model` model where model.sku = temp.sku)
              and no_ > @START
              limit @LIMIT
        """
        param = [{
            'field': 'START'
            , 'type': 'INTEGER'
            , 'value': str(start)
        }, {
            'field': 'LIMIT'
            , 'type': 'INTEGER'
            , 'value': str(limit)
        }]
        listSKU = self.bq_query(query=query, param=param)
        return listSKU

    def get_list_sku_with_model(self):
        query = """
              select
                ROW_NUMBER() OVER(order by sort desc, sku asc) as no_
                , ls.*
                , model.filename
                , model.r2_score_on_testset
                , model.r2_score_on_trainingset
              from 
                `stg.stock_list_sku` ls
                inner join `stg.stock_training_model` model on model.sku = ls.sku
                order by sort desc, sku asc
            ) as temp
            where
              no_ > @START
              limit @LIMIT 
        """

        param = [{
            'field': 'START'
            , 'type': 'INTEGER'
            , 'value': str(start)
        }, {
            'field': 'LIMIT'
            , 'type': 'INTEGER'
            , 'value': str(limit)
        }]
        listSKU = self.bq_query(query=query)
        return listSKU

    def generate_list_sku(self):
        categ = self.get_category_balance()
        listSKU = pd.DataFrame(columns=['sku', 'category_name_level_1', 'dev', 'qty', 'sort'])
        listSKU.set_index('sku', inplace=True)
        for index, row in categ.iterrows():
            query = """
                with sku as (
                  select
                    sku
                    , category_name_level_1
                    , min(date) as min_date
                    , max(date) as max_date
                  from
                    `stg.bi_daily_summary` summ
                  where
                    channel = 'ShoppingCart'
                    and category_name_level_1 = @CAT
                  group by
                    sku
                    , category_name_level_1
                ), max as 
                (
                  select 
                    max(date)
                  from
                    `stg.bi_daily_summary`  
                  where
                    channel = 'ShoppingCart'
                ), min as 
                (
                  select 
                    min(date)
                  from
                    `stg.bi_daily_summary`  
                  where
                    channel = 'ShoppingCart'
                )
                select
                  sku
                  , category_name_level_1
                  , ifnull(stddev(qty), 0) as dev
                  , sum(qty) as qty
                  , (5 - ifnull(stddev(qty), 0)) * sum(qty) *
                    (countif(qty <> 0) / 
                    (date_diff(date(max_date), date(min_date), DAY) + 1)) as sort
                from (
                  select
                    col.*
                    , ifnull(val.qty, 0) as qty
                  from (
                    select
                      *
                    from (
                      select
                        date
                      from
                        `stg.bi_datestream`
                        where date between (select * from min) and (select * from max)
                    ) as dt, sku
                  ) as col
                  left join (
                    select
                      date
                      , sku
                      , sum(qty) as qty
                    from
                      `stg.bi_daily_summary`
                    group by
                      date
                      , sku
                  ) as val on val.date = col.date
                    and val.sku = col.sku
                  where
                    col.date >= min_date
                ) as summ
                group by
                  sku
                  , category_name_level_1
                  , min_date
                  , max_date
                order by
                  sort desc
                  limit @LIMIT;
            """
            param = [{
                'field': 'CAT'
                , 'type': 'STRING'
                , 'value': row['category_name_level_1'].strip()
            }, {
                'field': 'LIMIT'
                , 'type': 'INTEGER'
                , 'value': str(row['portion'])
            }]
            ls = self.bq_query(query=query, param=param)
            listSKU = listSKU.append(ls, ignore_index=True, sort=True)
        listSKU['qty'] = listSKU['qty'].astype(int)
        return listSKU


if __name__ == "__main__":
    ds = data_bmd(10000)
    ls = ds.generate_list_sku()
    ls.sort_values('sort', ascending=False, inplace=True)
    ds.bq_insert(data=ls, table_name='stock_list_sku', index=['sku'])
