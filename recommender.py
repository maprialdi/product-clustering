from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import udf, monotonically_increasing_id
from pyspark.sql.types import StructType, StructField, StringType, ArrayType, DoubleType
import numpy as np
from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.linalg import Vectors
from pyspark.sql import functions as F
import math
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.sql.functions import lit


sc = SparkContext()
sqlContext = SQLContext(sc)

filename = "gs://bmd_recommendation/pivot_normalized_18"
filename_data_items="gs://bmd_recommendation/data_items_18"
filename_similiarity="gs://bmd_recommendation/similarity_block_18"

def read_file(filename):
    df = (sqlContext
          .read
          .format("csv")
          .options(header="true", maxColumns=110000)
          .load(filename)
          .na.drop())
    return df

def read_file_json(filename):
    df = (sqlContext
          .read
          .format("json")
          .options(header="true", maxColumns=110000, multiline="true")
          .load(filename)
          .na.drop())
    return df

def read_matrix_json(filename):
    df = (sqlContext
          .read
          .format("json")
          .options(header="true", maxColumns=110000)
          .load(filename)
          .na.drop())
    return df

def read_file_items(filename):
    data = read_file(filename)
    data_items_str = data.drop('customer_id')
    data.unpersist()
    magnitude_udf = udf(lambda row: math.sqrt(np.sum(np.square(row))), DoubleType())
    divider_udf = udf(lambda col, magnitude: col/magnitude, DoubleType())

    data_items=data_items_str.select(
        *(
            F.col(col).cast("double").alias(col)
            for col
            in data_items_str.columns
        )
    )

    columns=data_items_str.columns
    data_items_str.unpersist()

    assembler = VectorAssembler(
        inputCols=columns,
        outputCol="features")
    vectorized = assembler.transform(data_items)
    data_items.unpersist()
    vectorized_sum=vectorized.withColumn("magnitude", magnitude_udf(vectorized["features"]))
    vectorized_sum=vectorized_sum.drop('features')
    vectorized.unpersist()

    normalized=vectorized_sum.select(
        *(
            divider_udf(F.col(col), F.col('magnitude')).alias(col)
            for col
            in vectorized_sum.columns
        )
    )
    normalized=normalized.drop('magnitude')
    vectorized_sum.unpersist()
    return normalized

def calculate_similarity(data_items):
    data_row = data_items.rdd.map(
        lambda x: Vectors.dense(x))

    rmat=RowMatrix(data_row).columnSimilarities().toIndexedRowMatrix().rows.toDF()
    data_row.unpersist()
    return rmat

def generate_vector(size, indices, values):
    result=[]
    entries={}
    for x in range(0, len(indices)):
        index=indices[x]
        value=values[x]
        entries[index]=value

    for x in range(0, size):
        if x in indices:
            result.append(entries[x])
        else:
            result.append(0.0)
    return result

def dot_product(user, similarity):
    score=0.0
    for x,y in zip(user, similarity):
        score+=x*y
    return score

def get_known_user_likes(user):
    schema = StructType([StructField('customer_id', StringType(), True),
                         StructField('sku', StringType(), True)])

    purchases = (sqlContext
                 .read
                 .format("csv")
                 .schema(schema)
                 .options(header="true", delimiter=";")
                 .load("gs://bmd_recommendation/data18.csv")
                 .na.drop())

    purchases_user=purchases.limit(1000).filter(purchases.customer_id==user).select("sku").rdd.map(lambda x: x.sku).collect()
    purchases.unpersist()
    return purchases_user

def set_column_name(user_rating):
    names=user_rating.columns
    entries={}
    counter=0
    for name in names:
        entries[counter]=name
        counter+=1
    return entries

def calculate_score(user_rating, data_matrix, user):
    columns=user_rating.columns

    assembler = VectorAssembler(
        inputCols=columns,
        outputCol="features")
    vectorized = assembler.transform(user_rating).select("features")

    to_vector_udf = udf(lambda vector: vector.toArray().tolist(), ArrayType(DoubleType()))
    vector_udf = udf(generate_vector, ArrayType(DoubleType()))
    dot_udf=udf(dot_product, DoubleType())

    data_matrix_vectorized=data_matrix.withColumn("vector_span", vector_udf(F.col("vector.size"),F.col("vector.indices"),F.col("vector.values"))).select("index","vector_span")
    user_vectorized=vectorized.withColumn("vector", to_vector_udf(F.col("features"))).drop("features")
    crossed_df=data_matrix_vectorized.crossJoin(user_vectorized)
    data_matrix_score=crossed_df.withColumn("score", dot_udf(F.col("vector"),F.col("vector_span")))
    data_score=data_matrix_score.select("index","score")
    data_score_user=data_score.withColumn("user",lit(user))
    sum_score=data_score.select("score").groupBy().sum().collect()[0][0]
    data_score=data_score_user.withColumn("score", F.col("score")/sum_score).select("index","score").orderBy("score", ascending=False)

    crossed_df.unpersist()
    user_vectorized.unpersist()
    data_matrix_vectorized.unpersist()
    data_matrix_score.unpersist()

    purchases_list=get_known_user_likes(user)
    column_name_list=set_column_name(user_rating)

    rename_udf = udf(lambda idx: column_name_list.get(idx), StringType())
    data_score_renamed=data_score.withColumn("sku",rename_udf(F.col("index")))
    recommendation_list=data_score_renamed.filter(data_score_renamed.sku.isin(purchases_list)==False).drop("index").limit(20)

    data_score.unpersist()
    data_score_renamed.unpersist()
    return recommendation_list

def get_preference(user):
    data=read_file(filename)
    data_items=read_file_json(filename_data_items)
    data_matrix=read_matrix_json(filename_similiarity)
    data_index = data.select("*").withColumn("idx", monotonically_increasing_id())
    data_items_index=data_items.select("*").withColumn("idx", monotonically_increasing_id())
    user_data=data_index.filter(data_index["customer_id"]==user)

    user_index = user_data.select("idx")
    user_rating=data_items_index.join(user_index, ["idx"], "inner")

    score=calculate_score(user_rating.drop("idx"), data_matrix, user)
    return score

data_items = read_file_items(filename)
data_items.write.mode('overwrite').json(filename_data_items)

similarity=calculate_similarity(data_items)
similarity.write.mode('overwrite').json(filename_similiarity)
data_items.unpersist()
similarity.unpersist()

user='1130100000001'
result=get_preference(user)
result.write.mode('overwrite').json('gs://bmd_recommendation/recommendation_result_18')

sc.stop()