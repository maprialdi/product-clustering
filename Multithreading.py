import json
from ComputeSimilarity import main
from pyclustering.utils.metric import type_metric, distance_metric
import csv
import time
import numpy as np
import pandas as pd
from google.cloud.bigquery import SchemaField

from multiprocessing.pool import ThreadPool
from data import data_bmd
import threading

def readFile(input):
    with open(input) as f:
        sampleall = json.load(f)
    return sampleall

def distance(n, data):
    W=[]
    feature_distances=[]
    #print("running on CPU ", multiprocessing.current_process().name)
    user_function = lambda point1, point2: main(point1, point2);
    metric = distance_metric(type_metric.USER_DEFINED, func=user_function)
    for x in range(n,len(data)):
        d=data[n]
        distance, distances = metric(d, data[x])
        #rounded = round(distance, 3)
        W.append(distance)
        feature_distances.append(distances)
    return (n,W,feature_distances)

def yieldGenerator(r, i):
    row = []
    distances = r[1]
    dist_string = ','.join(map(str, distances))
    row.append(dist_string)
    row.append(i)
    yield row

def yieldGeneratorSampling(r, i, s):
    row = []
    distances = r[1]
    dist_string = ','.join(map(str, distances))
    row.append(dist_string)
    row.append(i)
    row.append(s)
    yield row

def calculateParallelStream(d, threads=4):
     #current_milli_time = lambda: int(round(time.time() * 1000))
     print("Calculating distance for each data...")
     pool = ThreadPool(threads)
     results=[]
     distance_qty=[]
     distance_desc=[]
     distance_price=[]
     distance_cat=[]
     indexes=list(range(0, len(d)))
     length=len(d)
     data_schema=SchemaField('data', 'STRING')
     index_schema=SchemaField('index', 'NUMERIC')
     ds = data_bmd(10000)
     ls=[]
     for i in indexes:
         time_start=current_milli_time()
         pct=float(i+1)/float(length)*100
         print("row number: ",i)
         pool = ThreadPool(processes=4)
         async_result = pool.apply_async(distance, (i, d))
         #r = threading.Thread(target=distance, args=(i,d))
         return_val = async_result.get()
         ds.bq_stream(data=yieldGenerator(return_val, i), table_name='stock_test', schema=[data_schema, index_schema])
         results.append(return_val)
         print("progress: ", pct, "%")
         time_finish = current_milli_time()
         print("Processing Time: ", time_finish - time_start)
     pool.close()
     pool.join()
     return results, distance_price, distance_qty, distance_cat, distance_desc


def calculateParallelStreamSampling(d, idx, threads=4):
    # current_milli_time = lambda: int(round(time.time() * 1000))
    print("Calculating distance for each data...")
    pool = ThreadPool(threads)
    results = []
    distance_qty = []
    distance_desc = []
    distance_price = []
    distance_cat = []
    indexes = list(range(0, len(d)))
    length = len(d)
    data_schema = SchemaField('data', 'STRING')
    row_schema = SchemaField('row', 'NUMERIC')
    index_schema = SchemaField('index', 'NUMERIC')
    ds = data_bmd(10000)
    ls = []
    for i in indexes:
        time_start=current_milli_time()
        pct = float(i + 1) / float(length) * 100
        print("row number: ", i)
        pool = ThreadPool(processes=4)
        async_result = pool.apply_async(distance, (i, d))
        # r = threading.Thread(target=distance, args=(i,d))
        return_val = async_result.get()
        ds.bq_stream(data=yieldGeneratorSampling(return_val, i, idx), table_name='stock_distance_sampling', schema=[data_schema, row_schema, index_schema])
        results.append(return_val)
        print("progress: ", pct, "%")
        time_finish = current_milli_time()
        print("Processing Time: ", time_finish - time_start)
    pool.close()
    pool.join()
    return results, distance_price, distance_qty, distance_cat, distance_desc

def calculateParallel(d, threads=4):
    pool = ThreadPool(threads)
    results = []
    distance_qty = []
    distance_desc = []
    distance_price = []
    distance_cat = []
    indexes = list(range(0, len(d)))
    length = len(d)
    data_schema = SchemaField('data', 'STRING')
    index_schema = SchemaField('index', 'NUMERIC')
    current_milli_time = lambda: int(round(time.time() * 1000))
    ds = data_bmd(10000)
    ls = []
    for i in indexes:
        pct = float(i + 1) / float(length) * 100
        print("row number: ", i)
        # time_start = current_milli_time()
        r = pool.starmap(distance, [(i, d)])
        #ds.bq_stream(data=yieldGenerator(r, i), table_name='stock_distance_data', schema=[data_schema, index_schema])
        # ls.append(row)
        #     for f in feature_distances:
        #         for key, value in f.items():
        #             distance_qty.append(f['qty'])
        #             distance_cat.append(f['category'])
        #             distance_desc.append(f['description'])
        #             distance_price.append(f['UnitPrice'])
        results.append(r)
        print("progress: ", pct, "%")
        # time_finish = current_milli_time()
        # print("Processing Time: ", time_finish - time_start)
    pool.close()
    pool.join()
    return results, distance_price, distance_qty, distance_cat, distance_desc


def queryMatrix():
    query = """
          select * 
          from 
            `stg.stock_test`
    """
    ds = data_bmd(10000)
    ls=ds.bq_query(query)
    sorted=ls.sort_values('index')
    mapped=sorted['data'].apply(lambda x: x.split(','))
    return mapped

def getMatrixBQ():
    time.sleep(60)
    print("Fetching Data...")
    pd=queryMatrix()
    length=len(pd)
    array_list = []
    for p in pd:
        array_list.extend(p)
    matrix = np.zeros((length, length))
    matrix[np.triu_indices(length, 0)] = np.array(array_list)
    return matrix

def queryMatrixSampling(no_prc):
    query = """
          select * 
          from 
            `stg.stock_distance_sampling`
          where index=@INDEX
    """

    param = [{
        'field': 'INDEX'
        , 'type': 'NUMERIC'
        , 'value': no_prc
    }]

    ds = data_bmd(10000)
    ls=ds.bq_query(query, param)
    sorted=ls.sort_values('index')
    mapped=sorted['data'].apply(lambda x: x.split(','))
    return mapped


def getMatrixBQSampling(no_prc):
    time.sleep(10)
    print("Fetching Data...")
    pd=queryMatrixSampling(no_prc)
    length=len(pd)
    array_list = []
    for p in pd:
        array_list.extend(p)
    matrix = np.zeros((length, length))
    matrix[np.triu_indices(length, 0)] = np.array(array_list)
    return matrix

def getMatrix(distance):
    length=len(distance)
    array_list = []
    for i in range(0,length):
        rowd=distance[i][0][1]
        array_list.extend(rowd)
    #arr_matrix=np.array(matrix)
    matrix = np.zeros((length, length))
    matrix[np.triu_indices(length, 0)] = np.array(array_list)
    #np.set_printoptions(formatter={'float_kind': '{:f}'.format})
    #print(matrix)
    return matrix

current_milli_time = lambda: int(round(time.time() * 1000))

def writeToFile(output, distance):
    file = open(output, 'w')
    for n in distance:
        wr = csv.writer(file, quoting=csv.QUOTE_NONE)
        wr.writerow(n)

def generate_distance_matrix(inputfile):
    no_threads=4
    data = readFile(inputfile)
    distance = calculateParallel(data, no_threads)
    distance_matrix = getMatrix(distance)
    square_matrix = distance_matrix.T + distance_matrix
    square_list = square_matrix.tolist()
    return square_list

def get_statistics(distances):
    dist_array=np.array(distances).astype(np.float)
    min=dist_array.min()
    max=dist_array.max()
    average=dist_array.mean()
    std_dev=dist_array.std()
    return min, max, average, std_dev

def write_statistics(distance_price, distance_qty, distance_cat, distance_desc):
    cols=['feature', 'min', 'max', 'mean', 'std deviation']
    features=['unitprice','quantity','category','description']
    distances=[distance_price, distance_qty, distance_cat, distance_desc]
    data=[]
    for d in zip(distances, features):
        f=d[1]
        elements=[f]
        dist=d[0]
        min,max,avg,std=get_statistics(dist)
        elements.extend([min,max,avg,std])
        data.append(elements)
    df=pd.DataFrame(data, columns=cols)
    df.to_csv("C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/distance_statistics-1.csv")

if __name__ == "__main__":
    no_threads=4
    inputfile="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/raw-1.json"
    outputfile="C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/distance_matrix_raw-1.csv"
    data = readFile(inputfile)
    time_start=current_milli_time()
    distance, distance_price, distance_qty, distance_cat, distance_desc = calculateParallel(data, no_threads)
    write_statistics(distance_price, distance_qty, distance_cat, distance_desc)
    distance_matrix=getMatrix(distance)
    square_matrix=distance_matrix.T + distance_matrix
    square_list=square_matrix.tolist()
    time_finish=current_milli_time()
    writeToFile(outputfile, square_matrix)
    print("processing time: ", time_finish-time_start)