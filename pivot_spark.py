from pyspark.sql.functions import min
from pyspark import SparkContext
from pyspark.sql import SQLContext
import pyspark.sql.functions as func
from pyspark.sql.functions import count, udf, col, struct
from pyspark.sql.types import StructType, StructField, StringType, IntegerType
from pyspark.sql.functions import lit
from pyspark.sql.functions import first
import numpy as np

sc = SparkContext()
sqlContext = SQLContext(sc)

schema = StructType([StructField('customer_id', StringType(), True),
					 StructField('sku', StringType(), True)])
					 #StructField('no_', StringType(), True),
                     #StructField('purchase', IntegerType(), True)])

purchases = (sqlContext
    .read
    .format("csv")
	.schema(schema)
    .options(header="true", delimiter=";")
    .load("gs://bmd_recommendation/data18.csv")
    .na.drop())

	
purchases=purchases.withColumn("purchase", lit(1))
#grouped = purchases.groupBy("Bill_to_Customer_No_","no_").agg(count("*").alias("cnt")).filter("cnt > 2")
#grouped=purchases.groupBy("Bill_to_Customer_No_").count().filter("count" > 1)
#grouped.write.csv('gs://bmd_recommendation/filtered_1718')

#schema = StructType([StructField('Bill_to_Customer_No_', StringType(), True),
#                     StructField('Count', IntegerType(), True)])

#filtered = (sqlContext
#    .read
#    .format("csv")
#	.schema(schema)
#    .options(header="false")
#    .load("gs://bmd_recommendation/filtered_1718")
#    .na.drop())

#p = purchases.alias("p")
#f = filtered.alias("f")
	
#joined=p.join(f, f.Bill_to_Customer_No_ == p.Bill_to_Customer_No_)
#result=joined.select('p.Bill_to_Customer_No_','no_','purchase')
#result.write.csv('gs://bmd_recommendation/compressed_1718')

purchases.registerTempTable("purchases")
sqlContext.cacheTable("purchases")

gexprs = ("customer_id")
aggexpr = min("purchase")

pivot=purchases.groupBy("customer_id").pivot("sku").agg(aggexpr).na.fill(0)

#magnitude_udf = udf(lambda row: np.sqrt(np.square(row).sum(axis=1)), IntegerType())
	#magnitude = np.sqrt(np.square(data_items).sum(axis=1))
	
#df_magnitude = pivot.withColumn("magnitude", magnitude_udf(struct([pivot[x] for x in pivot.columns])))
#df_normalized=df_magnitude.select([col(i)/df_magnitude.magnitude for i in df_magnitude.columns])

pivot.repartition(1).write.option("header", "true").csv('gs://bmd_recommendation/pivot_normalized_18')