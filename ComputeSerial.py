import json
from ComputeSimilarity import main
from pyclustering.utils.metric import type_metric, distance_metric;
import sys
import csv
import time
import numpy as np

def readFile(input):
    with open(input) as f:
        sampleall = json.load(f)
    return sampleall

def distance(n, data):
    W=[]
    user_function = lambda point1, point2: main(point1, point2)
    metric = distance_metric(type_metric.USER_DEFINED, func=user_function)
    for x in range(n,len(data)-1):
        d=data[n]
        distance = metric(d, data[x])
        W.append(distance)
    return (n,W)

def calculateSerial(d):
    results = []
    indices = list(range(0, len(d)))
    for index in indices:
        r = distance(index, d)
        results.append(r)
    return results

current_milli_time = lambda: int(round(time.time() * 1000))

def getMatrix(distance):
    length=len(distance)
    array_list = []
    for i in range(0,length):
        rowd=distance[i][0][1]
        array_list.extend(rowd)
    #arr_matrix=np.array(matrix)
    matrix = np.zeros((length, length))
    matrix[np.triu_indices(length, 0)] = np.array(array_list)
    #np.set_printoptions(formatter={'float_kind': '{:f}'.format})
    #print(matrix)
    return matrix


def writeToFile(output, distance):
    file = open(output, 'w')
    for n in distance:
        elements = n[1]
        wr = csv.writer(file, quoting=csv.QUOTE_ALL)
        wr.writerow(elements)
    file.close()

if __name__ == "__main__":
     no_threads=8
     inputfile="C:/Users/aprialdi.pratama/Downloads/sku_infoxx.json"
     outputfile="D:/data-bhinneka/result_multithreading.csv"
     data = readFile(inputfile)
     time_start=current_milli_time()
     distance = calculateSerial(data)
     time_finish=current_milli_time()
     writeToFile(outputfile, distance)
     print("processing time serial: ", time_finish-time_start)