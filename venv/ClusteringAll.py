import pandas as pd
from Multithreading import calculateParallelStream,getMatrix, getMatrixBQ, calculateParallel
from ClusteringMatrix import get_initial, clusteringMatrix
from ComputeSimilarity import main
from numpy import array
from pyclustering.utils.metric import type_metric, distance_metric
from collections import defaultdict
import json
import time
from data import data_bmd

def get_data():
    print("Fetching Data from BQ...")
    query = """
            select
                sku, case when UnitPrice <= 0 then 1 else UnitPrice end as UnitPrice, brand, name, description
                , trim(concat(Category_Name_Level_1, ' ', ifnull(Category_Name_Level_2, ''), ' ' , ifnull(Category_Name_Level_3, '') , ' ' , ifnull(Category_Name_Level_4, ''))) as category
                , qty
            from
                `stg.temp_cluster_week`
    """

    ds = data_bmd(100000)
    ls=ds.bq_query(query)
    return ls

def read_file(input):
    print("Reading File...")
    df=pd.read_json(input, lines=True)
    #print(len(df))
    return df

def get_medoids(medoids, sample):
    print("Getting initial medoids")
    result=[]
    for m in medoids:
        result.append(sample[m])
    return result

current_milli_time = lambda: int(round(time.time() * 1000))

def run_clustering(data, num_clusters):
    # distance, distance_price, distance_qty, distance_cat, distance_desc = calculateParallel(data)
    # distance_matrix=getMatrix(distance)
    distance, distance_price, distance_qty, distance_cat, distance_desc = calculateParallelStream(data)
    distance_matrix=getMatrixBQ()
    square_matrix=distance_matrix.T + distance_matrix
    square_list=square_matrix.tolist()
    matrix=array(square_list)
    initial = get_initial(int(num_clusters), len(matrix))
    clustering_instance = clusteringMatrix(matrix, initial)
    medoids = clustering_instance.get_medoids()
    return medoids

def get_membership(data, medoids, output):
    print("Calculating cluster membership")
    user_function = lambda point1, point2: main(point1, point2)
    metric = distance_metric(type_metric.USER_DEFINED, func=user_function)
    members=defaultdict(list)
    for d in data:
        min_distance=999999
        min_medoids=None
        for m in medoids:
            distance, features=metric(d,m)
            if distance<min_distance:
                min_distance=distance
                min_medoids=m
        members[min_medoids['sku']].append(d['sku'])
    cols=['medoid', 'members']
    elements=[]
    for medoid, member in members.items():
        elements.append((medoid, member))
    df=pd.DataFrame(elements,columns=cols)
    df.to_csv(output)

if __name__ == "__main__":
    time_start = current_milli_time()
    df=get_data()
    ts=time.time()
    outputFile="str(ts)"+".csv"
    #df=read_file("C:/Users/aprialdi.pratama/Documents/data-bhinneka-temp/raw_weekly.json")
    sampling_json=df.to_json(orient='records')
    data=json.loads(sampling_json)

    num_clusters=25

    medoids_final=get_medoids(run_clustering(data, num_clusters), data)
    get_membership(data,medoids_final, outputFile)
    time_finish = current_milli_time()
    print("Medoids: ",medoids_final)
    print("Processing Time: ", time_finish-time_start)